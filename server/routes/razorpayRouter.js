var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');
const Razorpay = require('razorpay');
const request = require('request');
const crypto = require('crypto');

const keys = require('../keys');
const Transaction = require('../models/Transactions');
const razorInstance = new Razorpay({
  key_id : keys.razorIdkey,
  key_secret : keys.razorIdSecret
});

const url = 'mongodb://localhost:27017/Razor';
const connect = mongoose.connect(url);

router.post("/order",(req,res) =>{
  try{
    const options = {
      amount : req.body.amount,
      currency : "INR",
      receipt : "receipt#1",
      payment_capture : 1
    }
    razorInstance.orders.create(options, function(err, order){
      if(err){
        return res.status(500).json({
          message : "Some errors!"
        });
      }
      return res.status(200).json(order);
    });
  }
  catch(err){
    return res.status(500).json({
      message : "Some errors!"
    });
  }
});
/*
router.post("/payment", (req,res) => {
  const generated_signature = crypto.createHmac('sha256', keys.razorIdSecret)
  generated_signature.update(req.body.razorpay_order_id+"|"+ req.body.transactionid)
  if ( generated_signature.digest('hex') === req.body.razorpay_signature){
    /*const transaction = new Transaction({
      transactionid:req.body.transactionid,
      transactionamount:req.body.transactionamount,
    });
    transaction.save(function(err, savedtransac){
      if(err){
        console.log("Problem");
        console.log(err);
        return res.status(500).send("Some Problem Occured");
      }
      res.send({transaction: savedtransac});
    });
    res.send('success');
  }
  else{
    return res.send('failed');
  }
});*/

router.post("/verification", (req,res)=>{
  const secret = '12345678';
  console.log(req.body);
  console.log(req.headers);
  console.log(req.body.payload.payment.entity);
	const shasum = crypto.createHmac('sha256', secret);
	shasum.update(JSON.stringify(req.body));
	const digest = shasum.digest('hex');


	if (digest === req.headers['x-razorpay-signature']) {
    connect.then((db) =>{
      const transaction = new Transaction({
        transactionid:req.body.payload.payment.entity.id,
        transactionamount:req.body.payload.payment.entity.amount,
        email: req.body.payload.payment.entity.email,
        contact: req.body.payload.payment.entity.contact
      });
      transaction.save(function(err, savedtransac){
        if(err){
          console.log("Problem");
          console.log(err);
          return res.status(500).send("Some Problem Occured");
        }
        console.log('request is legit');
        console.log(savedtransac);
		    res.json({ status: 'ok' });
      });
    })
	} else {
    return res.status(500).json({
      message : "Some errors!"
    });
	}
});
module.exports = router;
