const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Transactions = new Schema({
    transactionid:{
        type:String
    },
    transactionamount:{
        type:Number
    },
    email:{
        type:String
    },
    contact:{
        type:String
    }
});
module.exports = mongoose.model('Transaction', Transactions);